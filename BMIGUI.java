//********************************************************************
//  BMIGUI.java     
//
//  Wyznacza BMI w GUI.
//********************************************************************

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class BMIGUI
{
   private int WIDTH = 300;
   private int HEIGHT = 120;

   private JFrame frame;
   private JPanel panel;
   private JLabel wzrostLabel, wagaLabel, BMILabel, wynikLabel, wskazowka;
   private JTextField wzrost, waga;
   private JButton oblicz;

   //-----------------------------------------------------------------
   //  Ustawia GUI.
   //-----------------------------------------------------------------
   public BMIGUI()
   {
      frame = new JFrame ("Kalkulator BMI");
      frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

      //tworzenie etykiety dla pol tekstowych wzrostu i wagi 
      wzrostLabel = new JLabel ("Twoj wzrost w metrach:");
      wagaLabel = new JLabel ("Twoja waga w kilokramach: ");
      
      BMILabel = new JLabel ("To jest twoje BMI: ");
      wynikLabel = new JLabel("?.??"); //stworz etykiete wynik dla wartosci BMI
      
      
      wskazowka = new JLabel ("");
      
      wzrost = new JTextField(5);
      waga = new JTextField(5);
      // stworz JTextField dla wzrostu
      // stworz JTextField dla wagi

      // stworz przycisk, ktory po wcisnieciu policzy BMI
      // stworz BMIListener, ktory bedzie nasluchiwal czy przycis zostal nacisniety 
      
      oblicz = new JButton ("Policz swoje BMI");
      oblicz.addActionListener (new BMIListener());
      
      // ustawienia JPanel znajdujacego sie na JFrame 
      panel = new JPanel();
      panel.setPreferredSize (new Dimension(WIDTH, HEIGHT));
      panel.setBackground (Color.yellow);
      
      
      panel.add(wzrostLabel);
      panel.add(wzrost);
      panel.add(wagaLabel);
      panel.add(waga);
      panel.add(oblicz);
      panel.add(BMILabel);
      panel.add(wynikLabel);
      panel.add(wskazowka);
      
      //dodaj do panelu etykiete i pole tekstowe dla wzrostu
      //dodaj do panelu etykiete i pole tekstowe dla wagi
      //dodaj do panelu przycisk
      //dodaj do panelu etykiete BMI
      //dodaj do panelu etykiete dla wyniku

      //dodaj panel do frame 
      frame.getContentPane().add (panel);
      
   }

   //-----------------------------------------------------------------
   //  Wyswietl frame aplikacji podstawowej
   //-----------------------------------------------------------------
   public void display()
   {
      frame.pack();
      frame.show();
   }

   //*****************************************************************
   //  Reprezentuje action listenera dla przycisku oblicz.
   //*****************************************************************
   private class BMIListener implements ActionListener
   {
      //--------------------------------------------------------------
      //  Wyznacz BMI po wcisnieciu przycisku
      //--------------------------------------------------------------
      public void actionPerformed (ActionEvent event)
      {
         String wzrostText, wagaText;
         double wzrostVal, wagaVal;
	 double bmi;
	 String czyGruby;
	 
	 wzrostText = wzrost.getText();
	 wagaText = waga.getText();
	 wzrostVal = Double.parseDouble(wzrostText);
	 wagaVal = Double.parseDouble(wagaText);

	 //pobierz tekst z pol tekstowych dla wagi i wzrostu

	 //Wykorzystaj Integer.parseInt do konwersji tekstu na integer
	 
	 bmi = wagaVal / ( wzrostVal * wzrostVal );
	 
	 //bmi = 2;

	 //oblicz  bmi = waga / (wzrost)^2
	 
	 
	 java.text.DecimalFormat df=new java.text.DecimalFormat("0.00");
	 
	 wynikLabel.setText(df.format((double)bmi));
	 //Dodaj wynik do etykiety dla wyniku. 
	 // Wykorzystaj Double.toString do konwersji double na string.
	 
	 
	 if(bmi <= 19)
		czyGruby = "niedowaga" ;
	 else
		 if(bmi <= 25)
			 czyGruby = "waga prawid�owa" ;
		 else
			 if(bmi <= 30)
				 czyGruby = "nadwaga" ;
			 else
				 czyGruby = "oty�o��" ;
	 
	 wskazowka.setText(czyGruby);
      }
   }
}

