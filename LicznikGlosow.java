// ***********************************************************************
//  LicznikGlosow.java
//
//  Wykorzystuje GUI oraz listenery eventow do glosowania 
//  na dwoch kandydatow -- Jacka i Placka.
//
// ***********************************************************************

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LicznikGlosow extends JApplet
{
    private int APPLET_WIDTH = 300, APPLET_HEIGHT=100;
    private int glosyDlaJacek;
    private JLabel labelJacek;
    private JButton Jacek;
    
    private int glosyDlaJacek2;
    private JLabel labelJacek2;
    private JButton Jacek2;
    
    // ------------------------------------------------------------
    //  Ustawia GUI
    // ------------------------------------------------------------
    public void init ()
    {
	glosyDlaJacek = 0;
	
	Jacek = new JButton ("Glosuj na Jacka!");
	Jacek.addActionListener (new JacekButtonListener());
	
	Jacek2 = new JButton ("Glosuj na Placka!");
	Jacek2.addActionListener (new Jacek2ButtonListener());
	
	labelJacek = new JLabel ("Glosy dla Jacka: " + Integer.toString (glosyDlaJacek));
	labelJacek2 = new JLabel ("Glosy dla Placka: " + Integer.toString (glosyDlaJacek2));
	
	Container cp = getContentPane();
	cp.setBackground (Color.cyan);
	cp.setLayout (new FlowLayout());
	cp.add (Jacek);
	cp.add (labelJacek);
	
	cp.add (Jacek2);
	cp.add (labelJacek2);

	setSize (APPLET_WIDTH, APPLET_HEIGHT);
    }


    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku
    // *******************************************************************
    private class JacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
            glosyDlaJacek++;
            labelJacek.setText ("Glosy dla Jacka: " + Integer.toString (glosyDlaJacek)); 
            repaint ();
        }
    }
    
    private class Jacek2ButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
            glosyDlaJacek2++;
            labelJacek2.setText ("Glosy dla Placka: " + Integer.toString (glosyDlaJacek2)); 
            repaint ();
        }
    }
    
}

